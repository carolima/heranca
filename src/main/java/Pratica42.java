
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author João Paulo
 */
public class Pratica42 {
        
    
    public static void main(String[] args) {
        
        Elipse e1 = new Elipse(2.5, 3.5);
        Elipse e2 = new Elipse(2, 5);
        Circulo c1 = new Circulo(5);
        Circulo c2 = new Circulo(1.5);

        System.out.println(e1.getNome()+" 1\t: P = "+e1.getPerimetro()+" \t| A = "+e1.getArea());
        System.out.println(e2.getNome()+" 2\t: P = "+e2.getPerimetro()+" \t| A = "+e2.getArea()); 
        System.out.println(c1.getNome()+" 1\t: P = "+c1.getPerimetro()+" \t| A = "+c1.getArea());
        System.out.println(c2.getNome()+" 2\t: P = "+c2.getPerimetro()+" \t\t| A = "+c2.getArea());
    }
}
